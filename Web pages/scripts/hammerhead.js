
function controlLoop(){
	// Check for right motion
	if (m_right || k_right || p_right){

		document.getElementById("testfield").value = "Right";
		newAJAXCommand("fish.cgi?dir=0");
	}
	// Check for left motion
	else if (m_left || k_left || p_left){
		document.getElementById("testfield").value = "Left";
		newAJAXCommand("fish.cgi?dir=1");
	}
	// Check for forward motion
	else if (m_swim || k_swim || p_swim){
		document.getElementById("testfield").value = "Swim";
		newAJAXCommand("fish.cgi?dir=2");
	}
	// Else tell the fish to stop
	else {
		document.getElementById("testfield").value = "Stopped"
		newAJAXCommand("fish.cgi?dir=3");
	}

	pollAJAX();
	
}


//////////////////////// START Mouse Based Control //////////////////////////////////
var m_swim = 0;
var m_left = 0;
var m_right = 0;


function mouseButtonClick(e){
	if (e.target.id == "button_s"){
		m_swim = 1;
		controlLoop();
	}

	else if (e.target.id == "button_l"){
		m_left = 1;
		controlLoop();
	}

	else if (e.target.id == "button_r"){
		m_right = 1;
		controlLoop()
	}
}

function mouseButtonRelease(){
	m_swim = 0;
	m_left = 0;
	m_right = 0;
	controlLoop();
}

window.addEventListener("mousedown", function(event){ mouseButtonClick(event) } );
window.addEventListener("mouseup", mouseButtonRelease);

//////////////////////// END Mouse Based Control //////////////////////////////////


//////////////////////// START Key Based Control //////////////////////////////////

var k_swim = 0;
var k_left = 0;
var k_right = 0;

function keyDown(event){
	if (event.keyCode == 87){
		k_swim = 1;
		controlLoop();
	}

	else if (event.keyCode == 65){
		k_left = 1;
		controlLoop();
	}

	else if (event.keyCode == 68){
		k_right = 1;
		controlLoop();
	}
}

function keyUp(event){

	if (event.keyCode == 87){
		k_swim = 0;
		controlLoop();
	}

	else if (event.keyCode == 65){
		k_left = 0;
		controlLoop();
	}

	else if (event.keyCode == 68){
		k_right = 0;
		controlLoop();
	}

}

window.addEventListener("keydown", function(event){ keyDown(event) });
window.addEventListener("keyup", function(event){ keyUp(event) });

//////////////////////// END Key Based Control ////////////////////////////////////

//////////////////////// Start Pad Based Control ////////////////////////////////////

//Much of this code has been borrowed or is inspired from http://www.html5rocks.com/en/tutorials/doodles/gamepad/

// This only supports chrome right now. I had trouble getting firefox to recognise gamepads even on the site above.

// Some variables to represent the desired motion of the fish.
var p_swim = 0;
var p_left = 0;
var p_right = 0;

var gamepadConnected =0; // 1 if a gamepad is detected in slot [0], else 0;

// Checks if the browser supports webkit gamepads. Only chrome and maybe safari will pass this check,
// and start trying to detect gamepads.
if ( navigator.webkitGetGamepads) {
	window.requestAnimationFrame( detectGamepad );
}

// Runs a loop to see if a gamepad has been connected. I have only concerned myself with the first gamepad slot.
// This means that it will only work with the primary gamepad, while chrome seems to support up to 4.
// If gamepad [0] is disconnected, this tells pollGamepad to stop trying to read from it.
function detectGamepad(){
	if ( navigator.webkitGetGamepads()[0] != undefined ){
		window.requestAnimationFrame(pollGamepad);
	} else {
		window.requestAnimationFrame( detectGamepad);
	}

	
}

// This function checks if a gamepad is connected, and then checks the inputs if it is.
// It calls itself again next frame if one is connected too.
// If the gamepad has been disconnected, it exits without looping.
function pollGamepad(){
	if ( navigator.webkitGetGamepads()[0] != undefined ){

		gamepad = navigator.webkitGetGamepads()[0];

		if (gamepad.buttons[12] >= .5){
			p_swim = 1;
		} else {
			p_swim = 0;
		}

		if (gamepad.buttons[14] >= .5){
			p_left = 1;
		} else {
			p_left = 0;
		}

		if (gamepad.buttons[15] >= .5){
			p_right = 1;
		} else {
			p_right = 0;
		}

		controlLoop();
		window.requestAnimationFrame(pollGamepad);
	} else {
		p_swim = 0;
		p_left = 0;
		p_right = 0;
		controlLoop();
		window.requestAnimationFrame(detectGamepad);
	}
	
}



//////////////////////// END Pad Based Control ////////////////////////////////////
// Smart GIT test VERSION 2